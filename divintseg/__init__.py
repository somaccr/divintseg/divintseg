# Copyright (c) 2022 Darren Erik Vengroff

"""
Tools for computing diversity, integration, and segregation
metrics.
"""

from ._core import (
    diversity,
    integration,
    segregation,
    di,
)
